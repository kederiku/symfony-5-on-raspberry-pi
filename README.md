# Raspberry development environment for Symfony5

## Requirements:
* Docker
* Docker-compose

## Quick Start

* Run this command `docker-compose up -d`


## phpMyAdmin

to access phpMyAdmin, go to `localhost:8080`


## Use composer or npm

```bash
docker exec app-container composer --version
docker exec app-container npm --version
```

# Make or drop database
* drop database `php bin/console doctrine:database:drop --force`
* create database `php bin/console doctrine:database:create`


```bash
php bin/console doctrine:schema:drop --force
php bin/console doctrine:schema:create
php bin/console doctrine:fixture:load
```

## Tests
```bash
php bin/phpunit --testdox
php bin/phpunit --coverage-html var/test-coverage
```