<?php

namespace App\DataFixtures;

use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\User;
use Faker\Factory;

class UserFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        // init faker
        $faker = Factory::create('fr_FR');

        // User account Testing
        $user = new User();
        $user->setEmail('user@example.com');
        $password = $this->encoder->encodePassword($user, 'password');
        $user->setPassword($password);
        $manager->persist($user);

        // Admin account Testing
        $user = new User();
        $user->setEmail('admin@example.com');
        $password = $this->encoder->encodePassword($user, 'password');
        $user->setPassword($password);
        $user->setRoles(['ROLE_ADMIN']);
        $manager->persist($user);

        // Random users
        for ($count = 0; $count < 10; $count++) {
            $user = new User();
            $user->setEmail($faker->safeEmail());
            $password = $this->encoder->encodePassword($user, $faker->password());
            $user->setPassword($password);
            $manager->persist($user);
        }

        $manager->flush();
    }
}

