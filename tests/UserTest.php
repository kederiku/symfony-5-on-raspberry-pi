<?php

namespace App\Tests;

use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{

    public function testIsTrue(): void
    {
        $user = new User();

        $user->setEmail('true@test.com')
             ->setPassword('passwordTrue');

        $this->assertTrue($user->getEmail() === 'true@test.com');
        $this->assertTrue($user->getPassword() === 'passwordTrue');
    }

    public function testIsFalse(): void
    {
        $user = new User();

        $user->setEmail('true@test.com')
             ->setPassword('passwordTrue');

        $this->assertFalse($user->getEmail() === 'false@test.com');
        $this->assertFalse($user->getPassword() === 'passwordFalse');
    }

    public function testIsEmpty(): void
    {
        $user = new User();

        $this->assertEmpty($user->getEmail());
        $this->assertEmpty($user->getPassword());
    }

}